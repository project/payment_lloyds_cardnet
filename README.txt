Payment Lloyds Cardnet

INTRODUCTION
------------

Provides a the ability to setup a payment method using LLoyds Cardnet

REQUIREMENTS
------------

This module requires the following modules:

 * Payment (https://www.drupal.org/project/payment)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Create your payment method at admin/config/services/payment/method

MAINTAINERS
-----------

Current maintainers:
 * Liam Hiscock (LiamPower) - https://www.drupal.org/u/liampower
