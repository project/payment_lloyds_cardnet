<?php

/**
 * Contains PaymentPaymentLloydsCardnetController.
 */

/**
 * A Lloyds Cardnet payment controller.
 */
class PaymentLloydsCardnetController {

  /**
   * Hash the relevant data to make sure it can be verified.
   *
   * @param string $storename
   *   The storename given to you by Lloyds Cardnet.
   * @param string $transaction_date_time
   *   The time in the format Y:m:d-H:i:s.
   * @param int $amount
   *   Total cost.
   * @param string $currency
   *   The currency integer value.
   * @param string $secret
   *   The shared secret provided by Lloyds Cardnet.
   *
   * @return string
   *   The encoded hash value.
   */
  public static function hashAlgorithm($storename, $transaction_date_time, $amount, $currency, $secret) {
    $ascii = bin2hex($storename . $transaction_date_time . $amount . $currency . $secret);
    // SHA256 hash the bin2hex string.
    return hash('SHA256', $ascii);
  }

  /**
   * Gets the integer value of the currency and returns it.
   *
   * @param string $currency
   *   The string representation of the currency. e.g. GBP.
   *
   * @return bool|mixed
   *   If we find our currency then we return the currency number, else we
   *   return FALSE.
   */
  public static function getCurrencyNumber($currency) {
    $currency_mapping = array(
      'AUD' => 036,
      'BRL' => 986,
      'EUR' => 978,
      'INR' => 356,
      'GBP' => 826,
      'USD' => 840,
      'ZAR' => 710,
      'CHF' => 756,
      'BHD' => 048,
      'CAD' => 124,
      'CNY' => 156,
      'HRK' => 191,
      'CZK' => 203,
      'DKK' => 208,
      'HDK' => 344,
      'HUF' => 348,
      'ISL' => 376,
      'JPY' => 392,
      'KWD' => 414,
      'LTL' => 440,
      'MXN' => 484,
      'NZD' => 554,
      'NOK' => 578,
      'OMR' => 512,
      'PLN' => 985,
      'RON' => 946,
      'SAR' => 682,
      'SGD' => 702,
      'KRW' => 410,
      'SEK' => 752,
      'TRY' => 949,
      'AED' => 784,
    );

    if (isset($currency_mapping[$currency])) {
      return $currency_mapping[$currency];
    }
    return FALSE;
  }

  /**
   * Validates the contents of variables.
   *
   * @param array $variables
   *   The array of variables returned to us from Lloyds Cardnet.
   *
   * @return bool
   *   Validates the returned variables to make sure we have been approved.
   */
  public static function validate(array $variables) {
    if (isset($variables['invoicenumber'])) {
      $payment = entity_load_single('payment', filter_xss($variables['invoicenumber']));
      // Add our order ID from Lloyds Cardnet to the DB.
      if (isset($variables['oid']) && $oid = filter_xss($variables['oid'])) {
        db_insert('payment_lloyds_cardnet_payment')
          ->fields(array(
            'created' => time(),
            'pid' => $payment->pid,
            'oid' => $oid,
            'ref_no' => filter_xss($variables['refnumber']),
          ))->execute();
      }
      if ($variables['status'] == 'APPROVED' && $payment) {
        $statuses = PaymentLloydsCardnetController::statusMap();
        if ($variables['txntype'] == 'preauth') {
          $payment->setStatus(new PaymentStatusItem(PAYMENT_LLOYDS_CARDNET_STATUS_COMPLETED_AWATING_SUMBISSION));
        }
        else {
          $payment->setStatus(new PaymentStatusItem($statuses[$variables['status']]));
        }
        entity_save('payment', $payment);
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Formats the variables in to the array expected by Lloyds Cardnet.
   *
   * @param \Payment $payment
   *   The Payment object to use.
   * @param array $controller_data
   *   The $controller_data from $payment.
   *
   * @return array
   *   The completed payment fields array.
   */
  public static function paymentFields(Payment $payment, array $controller_data) {
    $currency = PaymentLloydsCardnetController::getCurrencyNumber($payment->currency_code);
    $transaction_date_time = new DateTime();

    // Prepare POST data.
    $return_url = url('payment-lloyds-cardnet/return', array(
      'absolute' => TRUE,
    ));
    $cancel_return_url = url('payment-lloyds-cardnet/return/cancel/' . $payment->pid . '/' . PaymentLloydsCardnetMethodController::hashPID($payment->pid), array(
      'absolute' => TRUE,
    ));

    // Set up our submit data.
    $data = array(
      'txntype' => $controller_data['paytype'],
      'timezone' => date_default_timezone(),
      'txndatetime' => $transaction_date_time->format('Y:m:d-H:i:s'),
      'hash_algorithm' => 'SHA256',
      'storename' => $controller_data['store_id'],
      'mode' => $controller_data['mode'],
      'currency' => $currency,
      'invoicenumber' => $payment->pid,
      'responseSuccessURL' => $return_url,
      'responseFailURL' => $cancel_return_url,
    );

    $amount = 0;
    // Add each line item separately. The first item starts at 1.
    foreach ($payment->line_items as $line_item) {
      $amount = $amount + $line_item->amount_total;
    }

    $data['chargetotal'] = $amount;
    // Now we have our amount we can generate the hash.
    $data['hash'] = PaymentLloydsCardnetController::hashAlgorithm($controller_data['store_id'], $transaction_date_time->format('Y:m:d-H:i:s'), $amount, $currency, $controller_data['shared_secret']);

    return $data;
  }

  /**
   * Returns a map of Lloyds Cardnet statuses to Payment statuses.
   *
   * @return array
   *   Keys are Lloyds Cardnet statuses, values are Payment statuses.
   */
  public static function statusMap() {
    return array(
      'APPROVED' => PAYMENT_STATUS_SUCCESS,
      'DECLINED' => PAYMENT_STATUS_FAILED,
      'FAILED' => PAYMENT_STATUS_FAILED,
    );
  }

}
