<?php

/**
 * Contains PaymentLloydsCardnetMethodController.
 */

/**
 * A Lloyds Cardnet payment method.
 */
class PaymentLloydsCardnetMethodController extends PaymentMethodController {

  /**
   * The Transaction type.
   */
  const MODE = 'payonly';

  /**
   * The production server URL.
   *
   * @var SERVER_URL
   */
  const SERVER_URL = 'https://www.ipg-online.com/connect/gateway/processing';

  /**
   * The sandbox server URL.
   *
   * @var SANDBOX_SERVER_URL
   */
  const SANDBOX_SERVER_URL = 'https://test.ipg-online.com/connect/gateway/processing';

  public $controller_data_defaults = array(
    'server' => self::SERVER_URL,
    'mode' => self::MODE,
  );

  public $payment_method_configuration_form_elements_callback = 'payment_lloyds_cardnet_payment_method_configuration_form_elements';

  /**
   * PaymentLloydsCardnetMethodController constructor.
   */
  function __construct() {
    $this->title = 'Lloyds Cardnet Payments';
  }

  /**
   * Implements PaymentMethodController::validate().
   */
  function validate(Payment $payment, PaymentMethod $payment_method, $strict) {
  }

  /**
   * Hashes a Payment PID.
   *
   * @param int $pid
   *   The PID value.
   *
   * @return string
   *   A SHA256 hash of the PID.
   */
  public static function hashPID($pid) {
    return hash('sha256', $pid . drupal_get_hash_salt());
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  function execute(Payment $payment) {
    entity_save('payment', $payment);
    $_SESSION['payment_lloyds_cardnet_pid'] = $payment->pid;
    drupal_goto('payment-lloyds-cardnet/redirect/' . $payment->pid);
  }

}
